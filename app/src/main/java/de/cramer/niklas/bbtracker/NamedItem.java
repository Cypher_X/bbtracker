package de.cramer.niklas.bbtracker;
interface NamedItem
{
	String getItemName();
}
