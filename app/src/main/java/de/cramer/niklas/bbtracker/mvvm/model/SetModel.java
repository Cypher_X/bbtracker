/*
 * Copyright (c) 2017.
 * Copyright by Niklas Cramer.
 ******************************************************************************/

package de.cramer.niklas.bbtracker.mvvm.model;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import de.niklascramer.mvvm.Property;
import de.niklascramer.mvvm.model.Model;
public class SetModel extends Model
{
	protected Property<Long>    exerciseIndex = new Property<>(-1L);
	protected Property<Integer> setIndex      = new Property<>(-1);
	protected Property<Long>    date          = new Property<>(-1L);
	protected Property<Integer> reps          = new Property<>(-1);
	protected Property<Float>   weight        = new Property<>(-1F);
	protected Property<Float>   workload      = new Property<>(-1F);
	protected Property<Long>    duration      = new Property<>(-1L);

	public SetModel(final ExerciseModel exerciseModel)
	{
		this();
		setDate(exerciseModel.getDate());
		setExerciseIndex(exerciseModel.id.get());
		exerciseModel.id.addListener(new PropertyChangeListener()
		{

			@Override
			public void propertyChange(final PropertyChangeEvent propertyChangeEvent)
			{
				id.set(exerciseModel.getId());
			}
		});
	}

	public SetModel()
	{
		super();
		setup();
	}

	@Override
	public void propertyChange(final PropertyChangeEvent evt)
	{
		super.propertyChange(evt);

		switch (evt.getPropertyName()) {
			case "weight":
			case "reps":
				calculateWorkload();
				break;
			default:
				break;
		}
	}

	public long getExerciseId()
	{
		return exerciseIndex.get();
	}

	public long getDate()
	{
		return date.get();
	}

	public void setDate(long date)
	{
		this.date.set(date);
	}

	public float getWeight()
	{
		return weight.get();
	}

	public int getReps()
	{
		return reps.get();
	}

	public float getWorkload()
	{
		return workload.get();
	}

	public int getSetIndex()
	{
		return setIndex.get();
	}

	public synchronized void setSetIndex(int setIndex)
	{
		this.setIndex.set(setIndex);
	}

	protected long getDuration()
	{
		return duration.get();
	}

	public synchronized void setDuration(final long duration)
	{
		this.duration.set(duration);
	}

	public synchronized void setWorkload(float workload)
	{
		this.workload.set(workload);
	}

	public void setReps(int reps)
	{
		this.reps.set(reps);
	}

	public void setWeight(float weight)
	{
		this.weight.set(weight);
	}

	private void calculateWorkload()
	{
		setWorkload(getWeight() * getReps());
	}

	public void setExerciseIndex(final long exerciseIndex)
	{
		this.exerciseIndex.set(exerciseIndex);
	}
}
