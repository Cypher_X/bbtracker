package de.cramer.niklas.bbtracker.fragment;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.os.Bundle;
import android.provider.BaseColumns;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.CursorAdapter;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import de.cramer.niklas.bbtracker.Exercise;
import de.cramer.niklas.bbtracker.R;
import de.cramer.niklas.bbtracker.mvvm.viewmodel.ExerciseViewModel;
import de.cramer.niklas.bbtracker.mvvm.viewmodel.ShellViewModel;

public class AddExerciseFragment extends CallbackDialogFragment<ExerciseViewModel>
{
	private SimpleCursorAdapter searchAdapter;

	@Nullable
	@Override
	public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable final Bundle savedInstanceState)
	{
		return inflater.inflate(R.layout.add_exercise_dialog, container, false);
	}

	@Override
	public void onViewCreated(@NonNull final View view, @Nullable final Bundle savedInstanceState)
	{
		super.onViewCreated(view, savedInstanceState);

		searchAdapter = new SimpleCursorAdapter(getActivity(), android.R.layout.simple_list_item_1, null, new String[]{"ExerciseName"}, new int[]{android.R.id.text1}, CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);

		final SearchView searchView = view.findViewById(R.id.exerciseNameSearch);
		searchView.setSuggestionsAdapter(searchAdapter);
		searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener()
		{

			@Override
			public boolean onQueryTextSubmit(final String s)
			{
				getViewModel().setExerciseName(s);
				return true;
			}

			@Override
			public boolean onQueryTextChange(final String s)
			{
				updateSuggestions(s);
				return true;
			}
		});
		searchView.setOnSuggestionListener(new SearchView.OnSuggestionListener()
		{

			@Override
			public boolean onSuggestionSelect(final int i)
			{
				return true;
			}

			@Override
			public boolean onSuggestionClick(final int i)
			{
				final Cursor cursor = searchAdapter.getCursor();
				cursor.moveToPosition(i);
				String suggestion = cursor.getString(1);
				searchView.setQuery(suggestion, true);
				return true;
			}
		});
	}

	private void updateSuggestions(final String s)
	{
		final List<Exercise> suggestions = ShellViewModel.config.exerciseRegistry.getSuggestions(s);
		final MatrixCursor cursor = new MatrixCursor(new String[]{BaseColumns._ID, "ExerciseName"});
		for (int i = 0; i < suggestions.size(); i++) {
			cursor.addRow(new Object[]{i, suggestions.get(i).getItemName()});
		}
		searchAdapter.changeCursor(cursor);
	}
}
