package de.cramer.niklas.bbtracker.mvvm.viewmodel;
import android.content.Context;

import org.joda.time.DateTime;

import de.cramer.niklas.bbtracker.Config;
import de.cramer.niklas.bbtracker.database.SQLiteDataBaseController;
import de.cramer.niklas.bbtracker.mvvm.AppMediator;
import de.niklascramer.mvvm.Property;
import de.niklascramer.mvvm.model.Model;
import de.niklascramer.mvvm.viewmodel.ListViewModel;
import de.niklascramer.mvvm.viewmodel.ViewModelBase;

public class ShellViewModel extends ViewModelBase
{
	private final Context context;

	private final Property<DateTime> currentTime = new Property<>(new DateTime());

	public static Config config;

	public SQLiteDataBaseController dataBase;

	public ShellViewModel(final Context context)
	{
		ListViewModel.viewModelFactory = new BBTrackerViewModelFactory(this);
		this.context = context;
		final SQLiteDataBaseController dataBase = new SQLiteDataBaseController(context);
		Model.dataBase = dataBase;
		this.dataBase = dataBase;
		mediator = new AppMediator();
		setup();
	}

	@Override
	protected void setup()
	{
		super.setup();
		config = new Config(context);
		config.init();
	}
}
