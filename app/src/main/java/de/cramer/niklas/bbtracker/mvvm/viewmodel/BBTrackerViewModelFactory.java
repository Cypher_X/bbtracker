package de.cramer.niklas.bbtracker.mvvm.viewmodel;
import de.cramer.niklas.bbtracker.mvvm.model.ExerciseModel;
import de.cramer.niklas.bbtracker.mvvm.model.WorkoutModel;
import de.niklascramer.mvvm.ViewModelFactory;
import de.niklascramer.mvvm.model.Model;
import de.niklascramer.mvvm.viewmodel.ViewModel;

public class BBTrackerViewModelFactory implements ViewModelFactory
{
	private final ShellViewModel shellViewModel;

	public BBTrackerViewModelFactory(final ShellViewModel shellViewModel)
	{
		this.shellViewModel = shellViewModel;
	}

	@Override
	public <V extends ViewModel<?>> V createViewModelForModel(final Model model)
	{
		V viewModel = null;

		if (model instanceof WorkoutModel) {
			WorkoutModel workoutModel = (WorkoutModel) model;
			viewModel = (V) new WorkoutViewModel(shellViewModel, workoutModel);
		}

		if (model instanceof ExerciseModel) {
			ExerciseModel exerciseModel = (ExerciseModel) model;
			viewModel = (V) new ExerciseViewModel(shellViewModel, exerciseModel);
		}
		return viewModel;
	}
}
