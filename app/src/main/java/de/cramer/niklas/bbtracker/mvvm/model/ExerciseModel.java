/*
 * Copyright (c) 2017.
 * Copyright by Niklas Cramer.
 ******************************************************************************/

package de.cramer.niklas.bbtracker.mvvm.model;

import org.joda.time.DateTime;

import java.beans.PropertyChangeListener;

import de.niklascramer.mvvm.Property;
import de.niklascramer.mvvm.model.ListModel;

public class ExerciseModel extends ListModel<SetModel>
{
	protected Property<Long>    date         = new Property<>(new DateTime().getMillis());
	protected Property<String>  exerciseType = new Property<>("");
	protected Property<String>  bodyPart     = new Property<>("");
	protected Property<Integer> listIndex    = new Property<>(-1);

	public Long getExericseId()
	{
		return exericseId.get();
	}

	public void setExericseId(final Long exericseId)
	{
		this.exericseId.set(exericseId);
	}

	protected Property<Long> exericseId = new Property<>(-1L);

	protected SetModel sumOfAllSets;

	public ExerciseModel(String name, DateTime date, PropertyChangeListener propertyChangedListener)
	{
		super(name, propertyChangedListener);
		this.date.set(date.getMillis());
		sumOfAllSets = new SetModel(this);
		add(sumOfAllSets);
		setup();
	}

	public ExerciseModel()
	{
		super();
		sumOfAllSets = new SetModel(this);
		add(sumOfAllSets);
		setup();
	}

	public Long getDate()
	{
		return date.get();
	}

	public ExerciseType getExerciseType()
	{
		return ExerciseType.valueOf(exerciseType.get());
	}

	public void setExerciseType(final String exerciseType)
	{
		this.exerciseType.set(exerciseType);
	}

	public Bodypart getBodyPart()
	{
		return Bodypart.valueOf(bodyPart.get());
	}

	public void setBodyPart(final String bodyPart)
	{
		this.bodyPart.set(bodyPart);
	}

	public int getExerciseIndex()
	{
		return listIndex.get();
	}

	public int getSetCount(){
		return getItems().size();
	}

	public void setExerciseIndex(final int exerciseIndex)
	{
		this.listIndex.set(exerciseIndex);
	}

	public void setId(final long id)
	{
		sumOfAllSets.setExerciseIndex(id);
	}

	public void setDate(final DateTime date)
	{
		this.date.set(date.getMillis());
	}

	private synchronized void updateSetSum()
	{
		int weightSum = 0;
		int repsSum = 0;
		int workloadSum = 0;

		for (final SetModel set : this) {
			weightSum += set.getWeight();
			repsSum += set.getReps();
			workloadSum += set.getWorkload();
		}

		sumOfAllSets.setWeight(weightSum);
		sumOfAllSets.setReps(repsSum);
		sumOfAllSets.setWorkload(workloadSum);
	}

	public SetModel getSumOfAllSets()
	{
		return sumOfAllSets;
	}

	public void setSumOfAllSets(SetModel sumOfAllSets)
	{
		this.sumOfAllSets = sumOfAllSets;
	}

	public void setModelSetSum(final SetModel setModelSum)
	{
		this.sumOfAllSets = setModelSum;
	}

	public int getNumSets()
	{
		return getItems().size() - 1;
	}

	public float getWorkload()
	{
		return sumOfAllSets.getWorkload();
	}

	public enum ExerciseType
	{
		WeightTraining,
		INIT,
		Cardio
	}

	public enum Bodypart
	{
		Chest,
		Back,
		Shoulders,
		Arms,
		Legs,
		UpperBody,
		INIT,
		LowerBodý
	}
}
