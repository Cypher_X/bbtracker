package de.cramer.niklas.bbtracker.fragment;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.View;

import de.cramer.niklas.bbtracker.R;
import de.cramer.niklas.bbtracker.common.DialogEventArgs;
import de.niklascramer.mvvm.DataBoundView;
import de.niklascramer.mvvm.viewmodel.ViewModel;
public abstract class CallbackDialogFragment<V extends ViewModel> extends DialogFragment implements DataBoundView<V>
{
	private V viewModel;

	public void setListener(final DialogCallback listener)
	{
		this.listener = listener;
	}

	private DialogCallback listener;

	protected Object[] values = new Object[0];

	protected void callback(DialogEventArgs args)
	{
		if (listener != null) {
			listener.onDialogResult(args);
		}
		dismiss();
	}

	@Override
	public void onViewCreated(@NonNull final View view, @Nullable final Bundle savedInstanceState)
	{
		super.onViewCreated(view, savedInstanceState);

		view.findViewById(R.id.okButton).setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View view)
			{
				callback(new DialogEventArgs(DialogResult.Ok, values));
			}
		});
		view.findViewById(R.id.cancelButton).setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View view)
			{
				callback(new DialogEventArgs(DialogResult.Cancel, values));
			}
		});
	}

	@Override
	public void setViewModel(final V viewModel)
	{
		this.viewModel = viewModel;
	}

	@Override
	public V getViewModel()
	{
		return viewModel;
	}

	public interface DialogCallback
	{
		void onDialogResult(DialogEventArgs args);
	}

	public enum DialogResult
	{
		Ok,
		Cancel
	}
}
