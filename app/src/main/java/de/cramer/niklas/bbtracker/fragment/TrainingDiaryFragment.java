package de.cramer.niklas.bbtracker.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.joda.time.DateTime;
import org.joda.time.Days;

import de.cramer.niklas.bbtracker.R;
import de.cramer.niklas.bbtracker.mvvm.AppMediator.GlobalEvents;
import de.cramer.niklas.bbtracker.mvvm.model.WorkoutModel;
import de.cramer.niklas.bbtracker.mvvm.viewmodel.ShellViewModel;
import de.cramer.niklas.bbtracker.mvvm.viewmodel.WorkoutViewModel;
import de.cramer.niklas.bbtracker.util.DateTimeChangedListener;
import de.cramer.niklas.bbtracker.util.DepthPageTransformer;
import de.niklascramer.mvvm.viewmodel.ViewModel;

@SuppressLint("ValidFragment")
public class TrainingDiaryFragment extends Fragment
{
	private final ShellViewModel shellViewmodel;

	public TrainingDiaryFragment(final ShellViewModel shellViewModel)
	{
		super();
		this.shellViewmodel = shellViewModel;
	}

	@Override
	public void onViewCreated(@NonNull final View view, @Nullable final Bundle savedInstanceState)
	{
		super.onViewCreated(view, savedInstanceState);

		ViewPager pager = view.findViewById(R.id.pager);
		pager.setPageTransformer(false, new DepthPageTransformer());
		pager.setAdapter(new SlidePagerAdapter(getChildFragmentManager(), pager));
		pager.setCurrentItem(bufferCount / 2);

		View actionButton = view.findViewById(R.id.actionButton);
		actionButton.setOnClickListener(new View.OnClickListener()
		{

			@Override
			public void onClick(final View view)
			{
				ViewModel.<GlobalEvents>getMediator().fireEvent(GlobalEvents.ActionButtonPressed);
			}
		});
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		return inflater.inflate(R.layout.fragment_training, container, false);
	}

	private class SlidePagerAdapter extends FragmentStatePagerAdapter implements DateTimeChangedListener
	{

		private final ViewPager pager;

		public SlidePagerAdapter(final FragmentManager fm, final ViewPager pager)
		{
			super(fm);
			this.pager = pager;
		}

		@Override
		public Fragment getItem(final int i)
		{
			final WorkoutModel workoutModel = new WorkoutModel();
			workoutModel.setDate(new DateTime().plusDays(i - bufferCount / 2).getMillis());

			final WorkoutViewModel workoutViewModel = new WorkoutViewModel(shellViewmodel, workoutModel);
			workoutViewModel.getExercises();

			final TrainingItemFragment trainingItemFragment = new TrainingItemFragment(workoutViewModel, null);
			trainingItemFragment.setDateTimeChangedListener(this);
			return trainingItemFragment;
		}

		@Override
		public int getCount()
		{
			return bufferCount;
		}

		@Override
		public void dateChanged(final DateTime dateTime)
		{
			final int item = bufferCount / 2 + Days.daysBetween(new DateTime(), dateTime).getDays();
			pager.setCurrentItem(item, true);
		}
	}

	private static int bufferCount = 360 * 100;
}
