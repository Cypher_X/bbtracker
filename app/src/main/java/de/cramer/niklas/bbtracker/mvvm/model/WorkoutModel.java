/*
 * Copyright (c) 2017.
 * Copyright by Niklas Cramer.
 ******************************************************************************/

package de.cramer.niklas.bbtracker.mvvm.model;

import java.beans.PropertyChangeListener;

import de.niklascramer.mvvm.Property;
import de.niklascramer.mvvm.model.ListModel;

public class WorkoutModel extends ListModel
{
	private Property<Long> date = new Property<>(-1L);

	public WorkoutModel()
	{
		setup();
	}

	public WorkoutModel(String title, PropertyChangeListener propertyChangedListener, long date)
	{
		super(title, propertyChangedListener);
		this.date.set(date);
		setup();
	}

	public long getDate()
	{
		return date.get();
	}

	public void setDate(long date)
	{
		this.date.set(date);
	}
}
