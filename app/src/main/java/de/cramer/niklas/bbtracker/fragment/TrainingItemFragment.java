package de.cramer.niklas.bbtracker.fragment;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.TextView;

import org.joda.time.DateTime;

import java.beans.PropertyChangeEvent;

import de.cramer.niklas.bbtracker.R;
import de.cramer.niklas.bbtracker.common.DialogEventArgs;
import de.cramer.niklas.bbtracker.mvvm.AppMediator;
import de.cramer.niklas.bbtracker.mvvm.model.ExerciseModel;
import de.cramer.niklas.bbtracker.mvvm.viewmodel.ExerciseViewModel;
import de.cramer.niklas.bbtracker.mvvm.viewmodel.WorkoutViewModel;
import de.cramer.niklas.bbtracker.util.DateTimeChangedListener;
import de.cramer.niklas.bbtracker.view.ExerciseListItemView;
import de.niklascramer.mvvm.mediator.Mediator;
import de.niklascramer.mvvm.mediator.Mediator.MediatorCallback;
import de.niklascramer.mvvm.viewmodel.ViewModel;
import de.niklascramer.mvvm.viewmodel.ViewModelBinding;
import de.niklascramer.util.collections.CollectionChangedListener;
import de.niklascramer.util.collections.ObservableCollection;

public class TrainingItemFragment extends BaseFragment<WorkoutViewModel>
{
	private TextView dateTextView;

	private ViewGroup exerciseList;

	private View busyIndicator;

	private DateTimeChangedListener dateTimeChangedListener;

	public TrainingItemFragment()
	{
		super();
	}

	@SuppressLint("ValidFragment")
	public TrainingItemFragment(final WorkoutViewModel viewModel, final Bundle args)
	{
		super(viewModel, args);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		return inflater.inflate(R.layout.fragment_training_item, container, false);
	}

	@Override
	public void onViewCreated(@NonNull final View view, @Nullable final Bundle savedInstanceState)
	{
		super.onViewCreated(view, savedInstanceState);

		exerciseList = view.findViewById(R.id.exerciseList);

		dateTextView = view.findViewById(R.id.trainingItemDate);
		dateTextView.setOnClickListener(new View.OnClickListener()
		{

			@Override
			public void onClick(final View view)
			{
				showPickDateDialog(viewModel);
			}
		});
		busyIndicator = view.findViewById(R.id.busyIndicator);

		// Databinding
		viewModel.addBinding(new ViewModelBinding()
		{
			@Override
			public void executeBinding(final PropertyChangeEvent event)
			{
				dateTextView.setText(viewModel.getDate());
			}
		});

		for (ExerciseViewModel exercise : viewModel) {
			addExercise(exercise);
		}

		viewModel.addListListener(new CollectionChangedListener<ExerciseViewModel>()
		{
			@Override
			public void onListChanged(final Object sender, final ExerciseViewModel element, final int index, final ObservableCollection.ListEvent action)
			{
				switch (action) {

					case Add:
						addExercise(element);
						break;
					case Clear:
						break;
					case Remove:
						break;
				}
			}
		});

		final Mediator<AppMediator.GlobalEvents> mediator = ViewModel.getMediator();
		mediator.listen(AppMediator.GlobalEvents.ActionButtonPressed, actionButtonPressed);
		mediator.listen(AppMediator.GlobalEvents.IsBusy, isBusyChanged);
	}

	public MediatorCallback actionButtonPressed = new MediatorCallback()
	{
		@Override
		public void callback(final Object value)
		{
			if (getUserVisibleHint()) {
				addExercise();
			}
		}
	};

	public MediatorCallback isBusyChanged = new MediatorCallback<Boolean>()
	{
		@Override
		public void callback(final Boolean value)
		{
			busyIndicator.setVisibility(value ? View.VISIBLE : View.GONE);
		}
	};

	private void addExercise()
	{
		final ExerciseViewModel exerciseViewModel = new ExerciseViewModel(viewModel.shell, new ExerciseModel());
		final CallbackDialogFragment dialogFragment = showAddExerciseDialog(exerciseViewModel);
		dialogFragment.setListener(new CallbackDialogFragment.DialogCallback()
		{

			@Override
			public void onDialogResult(final DialogEventArgs args)
			{
				switch (args.getDialogResult()) {
					case Ok:
						addExercise(exerciseViewModel);
						break;
					case Cancel:
						break;
				}
			}
		});
	}

	private CallbackDialogFragment<ExerciseViewModel> showAddExerciseDialog(final ExerciseViewModel exerciseViewModel)
	{
		AddExerciseFragment addExerciseFragment = new AddExerciseFragment();
		addExerciseFragment.setViewModel(exerciseViewModel);
		addExerciseFragment.show(getFragmentManager(), ExerciseViewModel.class.getSimpleName());
		return addExerciseFragment;
	}

	private CallbackDialogFragment showPickDateDialog(final WorkoutViewModel viewModel)
	{

		DateTime time = new DateTime(viewModel.getModel().getDate());

		DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener()
		{
			@Override
			public void onDateSet(final DatePicker datePicker, final int year, final int month, final int day)
			{
				DateTime dateTime = new DateTime().withYear(year).withMonthOfYear(month + 1).withDayOfMonth(day);

				if (dateTimeChangedListener != null) {
					dateTimeChangedListener.dateChanged(dateTime);
				}
			}
		};

		DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), dateSetListener, time.getYear(), time.getMonthOfYear() - 1, time.getDayOfMonth());
		datePickerDialog.show();

		return null;
	}

	private void addExercise(final ExerciseViewModel element)
	{
		addView(element);
	}

	private void addView(final ExerciseViewModel exerciseViewModel)
	{
		final ExerciseListItemView exerciseListItem = (ExerciseListItemView) getLayoutInflater().inflate(R.layout.exercise_list_item, (ViewGroup) getView(), false);
		exerciseListItem.setViewModel(exerciseViewModel);
		exerciseList.addView(exerciseListItem);
	}

	public void setDateTimeChangedListener(final DateTimeChangedListener dateTimeChangedListener)
	{
		this.dateTimeChangedListener = dateTimeChangedListener;
	}
}
