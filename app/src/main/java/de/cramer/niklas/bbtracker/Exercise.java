package de.cramer.niklas.bbtracker;
public class Exercise implements NamedItem
{
	private String exerciseName;

	private String[] trainedMuscles;

	public String getExerciseName()
	{
		return exerciseName;
	}

	public void setExerciseName(final String exerciseName)
	{
		this.exerciseName = exerciseName;
	}

	public String[] getTrainedMuscles()
	{
		return trainedMuscles;
	}

	public void setTrainedMuscles(final String[] trainedMuscles)
	{
		this.trainedMuscles = trainedMuscles;
	}

	@Override
	public String getItemName()
	{
		return getExerciseName();
	}
}
