package de.cramer.niklas.bbtracker.mvvm;
import de.niklascramer.mvvm.mediator.Mediator;

public class AppMediator extends Mediator<Enum<?>>
{
	public enum GlobalEvents
	{
		ActionButtonPressed,
		IsBusy
	}
}
