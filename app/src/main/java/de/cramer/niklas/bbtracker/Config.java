package de.cramer.niklas.bbtracker;
import android.content.Context;

import com.eclipsesource.json.Json;
import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;
import com.eclipsesource.json.JsonValue;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

public class Config
{
	private final Context context;

	public ExerciseRegistry exerciseRegistry = new ExerciseRegistry();

	private Object syncRoot = new Object();

	public Config(final Context context)
	{
		this.context = context;
	}

	public void init()
	{
		synchronized(syncRoot) {
			initExerciseRegistry();
		}
	}

	private void initExerciseRegistry()
	{
		try {
			InputStream inputStream = context.getAssets().open("exercises.json");
			Reader reader = new InputStreamReader(inputStream);
			JsonArray parsed = Json.parse(reader).asArray();

			for (JsonValue jsonValue : parsed) {
				addExercise(jsonValue.asObject());
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void addExercise(final JsonObject json)
	{
		Exercise exercise = new Exercise();
		exercise.setExerciseName(json.getString("exerciseName", ""));
		final JsonArray muscles = json.get("trainedMuscles").asArray();
		String[] musclesString = new String[muscles.size()];
		for (int i = 0; i < muscles.size(); i++) {
			musclesString[i] = muscles.get(i).toString();
		}
		exercise.setTrainedMuscles(musclesString);
		exerciseRegistry.addItem(exercise);
	}
}
