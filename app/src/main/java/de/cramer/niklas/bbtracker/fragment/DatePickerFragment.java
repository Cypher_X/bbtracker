package de.cramer.niklas.bbtracker.fragment;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CalendarView;

import de.cramer.niklas.bbtracker.R;
public class DatePickerFragment extends CallbackDialogFragment
{
	@Nullable
	@Override
	public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable final Bundle savedInstanceState)
	{
		return inflater.inflate(R.layout.date_picker, container, false);
	}

	@Override
	public void onViewCreated(@NonNull final View view, @Nullable final Bundle savedInstanceState)
	{
		super.onViewCreated(view, savedInstanceState);
		CalendarView calendarView = view.findViewById(R.id.calendarView);
		calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener()
		{

			@Override
			public void onSelectedDayChange(@NonNull final CalendarView calendarView, final int i, final int i1, final int i2)
			{
				final long date = calendarView.getDate();
				values = new Object[]{date};
			}
		});
	}
}