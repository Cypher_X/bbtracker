/*
 * Copyright (c) 2017.
 * Copyright by Niklas Cramer.
 ******************************************************************************/

package de.cramer.niklas.bbtracker.mvvm.model;

import java.beans.PropertyChangeListener;

import de.niklascramer.mvvm.Property;
import de.niklascramer.mvvm.model.Model;

/**
 * Created by Niklas on 20.07.2017.
 */

public class FoodItem extends Model
{
	private Property<Float> protein = new Property<>(-1F);
	private Property<Float> carbs   = new Property<>(-1F);
	private Property<Float> fat     = new Property<>(-1F);

	private Property<Float> natrium   = new Property<>(-1F);
	private Property<Float> potassium = new Property<>(-1F);

	protected FoodItem(final String title, final PropertyChangeListener propertyChangedListener)
	{
		super(title, propertyChangedListener);
	}

	protected FoodItem()
	{
	}

	protected float getProtein()
	{
		return protein.get();
	}

	protected void setProtein(final float protein)
	{
		this.protein.set(protein);
	}

	protected float getCarbs()
	{
		return carbs.get();
	}

	protected void setCarbs(final float carbs)
	{
		this.carbs.set(carbs);
	}

	protected float getFat()
	{
		return fat.get();
	}

	protected void setFat(final float fat)
	{
		this.fat.set(fat);
	}

	protected float getNatrium()
	{
		return natrium.get();
	}

	protected void setNatrium(final float natrium)
	{
		this.natrium.set(natrium);
	}
}
