package de.cramer.niklas.bbtracker;
import java.util.ArrayList;
import java.util.List;

public class Registry<T extends NamedItem>
{
	private List<T> entries = new ArrayList<>();

	public T getItemByName(String name)
	{
		for (T entry : entries) {
			if (entry.getItemName().equalsIgnoreCase(name)) {
				return entry;
			}
		}
		return null;
	}

	public List<T> getSuggestions(String input)
	{
		List<T> result = new ArrayList<>();
		for (T entry : entries) {
			if (entry.getItemName().toLowerCase().startsWith(input.toLowerCase())) {
				result.add(entry);
			}
		}
		return result;
	}

	public void addItem(final T item)
	{
		entries.add(item);
	}
}
