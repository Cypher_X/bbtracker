/*
 * Copyright (c) 2017.
 * Copyright by Niklas Cramer.
 ******************************************************************************/

package de.cramer.niklas.bbtracker.mvvm.viewmodel;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.List;

import SQLite.IProgressListener;
import de.cramer.niklas.bbtracker.mvvm.model.ExerciseModel;
import de.cramer.niklas.bbtracker.mvvm.model.WorkoutModel;
import de.niklascramer.mvvm.Property;
import de.niklascramer.mvvm.viewmodel.ListViewModel;
/**
 * Created by Niklas on 30.06.2017.
 */

public class WorkoutViewModel extends ListViewModel<ExerciseViewModel, WorkoutModel>
{
	public final ShellViewModel shell;

	private Property<Boolean> isBusyProperty = new Property<>(false);

	private DateTimeFormatter fmt = DateTimeFormat.forPattern("dd.MM.YYYY");

	public WorkoutViewModel(final ShellViewModel shellViewmodel, final WorkoutModel model)
	{
		super(model);
		this.shell = shellViewmodel;
	}

	public String getDate()
	{
		return fmt.print(model.getDate());
	}

	public void setDate(final int date)
	{
		model.setDate(date);
	}

	public void getExercises()
	{
		final List<ExerciseModel> exercises = shell.dataBase.getExercisesForDay(model.getDate(), new IProgressListener()
		{

			@Override
			public void onProgressChanged(final Object sender, final int progress)
			{

			}
		});

		for (ExerciseModel exercise : exercises) {
			model.add(exercise);
		}
	}
}
