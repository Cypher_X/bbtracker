/*
 * Copyright (c) 2017.
 * Copyright by Niklas Cramer.
 ******************************************************************************/

package de.cramer.niklas.bbtracker.mvvm.viewmodel;

import java.text.DecimalFormat;

import de.cramer.niklas.bbtracker.mvvm.model.SetModel;
import de.niklascramer.mvvm.viewmodel.ViewModel;

/**
 * Created by Niklas on 19.06.2017.
 */

public class SetViewModel extends ViewModel<SetModel>
{
	private static DecimalFormat decimalFormat = new DecimalFormat("#.##");

	public SetViewModel(final SetModel setModel)
	{
		super(setModel);
	}

	public String getWeightText()
	{
		return decimalFormat.format(getWeight());
	}

	public float getWeight()
	{
		return model.getWeight();
	}

	public void setWeight(final float weight)
	{
		model.setWeight(weight);
	}

	public String getRepsText()
	{
		return getReps() + "";
	}

	public int getReps()
	{
		return model.getReps();
	}

	public void setReps(int count)
	{
		model.setReps(count);
	}

	public int getSetIndex()
	{
		return model.getSetIndex();
	}

	public void addRep()
	{
		model.setReps(model.getReps() + 1);
	}

	public void decreaseRep()
	{
		int value = model.getReps() - 1;
		if (value >= 0) {
			model.setReps(value);
		}
		else {
			model.setReps(0);
		}
	}

	public String getWorkloadText()
	{
		return decimalFormat.format(getWorkload()) + " kG";
	}

	private float getWorkload()
	{
		return model.getWorkload();
	}

	public void deleteSet()
	{
		model.delete();
	}

	public void setDate(final long date)
	{
		model.setDate(date);
	}
}
