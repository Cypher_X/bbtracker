package de.cramer.niklas.bbtracker.util;
import org.joda.time.DateTime;

public interface DateTimeChangedListener
{
	void dateChanged(DateTime dateTime);
}
