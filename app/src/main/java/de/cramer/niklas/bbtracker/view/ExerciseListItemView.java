package de.cramer.niklas.bbtracker.view;
import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.beans.PropertyChangeEvent;

import de.cramer.niklas.bbtracker.R;
import de.cramer.niklas.bbtracker.mvvm.viewmodel.ExerciseViewModel;
import de.niklascramer.mvvm.DataBoundView;
import de.niklascramer.mvvm.viewmodel.ViewModelBinding;

public class ExerciseListItemView extends LinearLayout implements DataBoundView<ExerciseViewModel>
{
	private ExerciseViewModel viewModel;

	private TextView exerciseName, setCount;

	public ExerciseListItemView(final Context context)
	{
		super(context);
	}

	public ExerciseListItemView(final Context context, @Nullable final AttributeSet attrs)
	{
		super(context, attrs);
	}

	public ExerciseListItemView(final Context context, @Nullable final AttributeSet attrs, final int defStyleAttr)
	{
		super(context, attrs, defStyleAttr);
	}

	public ExerciseListItemView(final Context context, final AttributeSet attrs, final int defStyleAttr, final int defStyleRes)
	{
		super(context, attrs, defStyleAttr, defStyleRes);
	}

	@Override
	protected void onFinishInflate()
	{
		super.onFinishInflate();

		exerciseName = findViewById(R.id.exerciseName);
		setCount = findViewById(R.id.exerciseSetCount);
	}

	@Override
	public void setViewModel(final ExerciseViewModel viewModel)
	{
		this.viewModel = viewModel;
		viewModel.addBinding(new ViewModelBinding()
		{
			@Override
			public void executeBinding(final PropertyChangeEvent event)
			{
				exerciseName.setText(viewModel.getExerciseName());
				setCount.setText(viewModel.getNumSets() + "");
			}
		});
	}

	@Override
	public ExerciseViewModel getViewModel()
	{
		return viewModel;
	}
}
