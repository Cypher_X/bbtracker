/*
 * Copyright (c) 2017.
 * Copyright by Niklas Cramer.
 ******************************************************************************/

package de.cramer.niklas.bbtracker.mvvm.viewmodel;

import org.joda.time.DateTime;

import de.cramer.niklas.bbtracker.mvvm.model.ExerciseModel;
import de.niklascramer.mvvm.viewmodel.ListViewModel;

public class ExerciseViewModel extends ListViewModel<SetViewModel, ExerciseModel>
{
	private final ShellViewModel shellViewModel;

	public ExerciseViewModel(final ShellViewModel shellViewModel, ExerciseModel attachedModel)
	{
		super(attachedModel);
		model = attachedModel;
		this.shellViewModel = shellViewModel;
	}

	public long getDate()
	{
		return model.getDate();
	}

	public void setDate(final DateTime date)
	{
		model.setDate(date);
	}

	public String getExerciseName()
	{
		return model.getName();
	}

	public void setExerciseName(final String exerciseName)
	{
		model.setName(exerciseName);
	}

	public long getId()
	{
		return model.getId();
	}

	public int getNumSets()
	{
		return model.getNumSets();
	}

	public String getWorkload()
	{
		return String.valueOf(model.getWorkload());
	}

	public long getExerciseIndex()
	{
		return model.getExerciseIndex();
	}
}
