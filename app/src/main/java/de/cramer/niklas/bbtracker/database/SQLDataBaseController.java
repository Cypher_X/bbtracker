/*
 * Copyright (c) 2018.
 * Copyright by Niklas Cramer.
 ******************************************************************************/

package de.cramer.niklas.bbtracker.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import SQLite.IDataBase;

import de.niklascramer.controller.Controller;
import de.niklascramer.mvvm.model.Model;

/**
 * Created by Niklas on 09.03.2018.
 */

public class SQLDataBaseController extends Controller implements IDataBase
{

	public static String host = "81.169.143.208";

	private static SQLDataBaseController instance;

	public SQLDataBaseController( )

	{
		try {
			Connection connection = DriverManager.getConnection(host, "niklas", "niklas");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static SQLDataBaseController getInstance()
	{
		if (instance == null) {
			instance = new SQLDataBaseController();
		}

		return instance;
	}

	@Override
	public void pause()
	{

	}

	@Override
	public void resume()
	{

	}

	@Override
	public void deleteFromDataBase(final Model model)
	{

	}

	@Override
	public void saveToDatabase(final Model model)
	{

	}
}
