package de.cramer.niklas.bbtracker.activity;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import de.cramer.niklas.bbtracker.R;
import de.cramer.niklas.bbtracker.fragment.MainFragment;
import de.cramer.niklas.bbtracker.fragment.TrainingDiaryFragment;
import de.cramer.niklas.bbtracker.mvvm.viewmodel.ShellViewModel;
import de.niklascramer.ui.dispatcher.UIDispatcher;

public class MainActivity extends AppCompatActivity
{
	private Handler handler = new Handler();

	private ShellViewModel shellViewModel;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		UIDispatcher.getInstance().setDispatcher(new UIDispatcher()
		{

			@Override
			public void postOnUI(final Runnable runnable)
			{
				handler.post(runnable);
			}
		});

		setContentView(R.layout.activity_main);

		shellViewModel = new ShellViewModel(this);

		getSupportFragmentManager().beginTransaction().add(R.id.mainContent, new MainFragment()).commit();
	}

	public void openTrainingView(View view)
	{
		final FragmentManager supportFragmentManager = getSupportFragmentManager();

		final FragmentTransaction fragmentTransaction = supportFragmentManager.beginTransaction();

		final TrainingDiaryFragment trainingDiaryFragment = new TrainingDiaryFragment(shellViewModel);
		fragmentTransaction.replace(R.id.mainContent, trainingDiaryFragment);
		fragmentTransaction.addToBackStack(null);
		fragmentTransaction.commit();
	}
}