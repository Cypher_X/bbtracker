package de.cramer.niklas.bbtracker.fragment;
import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import de.niklascramer.mvvm.DataBoundView;
import de.niklascramer.mvvm.viewmodel.ViewModel;

@SuppressLint("ValidFragment")
public abstract class BaseFragment<V extends ViewModel> extends Fragment implements DataBoundView<V>
{
	protected V viewModel;

	public BaseFragment()
	{

	}

	public BaseFragment(final V viewModel, Bundle args)
	{
		super();
		this.viewModel = viewModel;
	}

	public V getViewModel()
	{
		return viewModel;
	}

	public void setViewModel(final V viewModel)
	{
		this.viewModel = viewModel;
	}
}
