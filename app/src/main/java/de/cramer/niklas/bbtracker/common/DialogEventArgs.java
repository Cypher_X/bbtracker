package de.cramer.niklas.bbtracker.common;
import de.cramer.niklas.bbtracker.fragment.CallbackDialogFragment;

public  class DialogEventArgs
{
	private CallbackDialogFragment.DialogResult dialogResult;
	private Object[]                            data;

	public DialogEventArgs(final CallbackDialogFragment.DialogResult dialogResult, final Object[] data)
	{
		this.dialogResult = dialogResult;
		this.data = data;
	}

	public CallbackDialogFragment.DialogResult getDialogResult()
	{
		return dialogResult;
	}

	public Object[] getData()
	{
		return data;
	}
}
