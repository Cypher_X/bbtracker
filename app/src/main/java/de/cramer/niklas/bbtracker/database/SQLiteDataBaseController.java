/*
 * Copyright (c) 2017.
 * Copyright by Niklas Cramer.
 ******************************************************************************/

package de.cramer.niklas.bbtracker.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import SQLite.IDataBase;
import SQLite.IProgressListener;
import de.cramer.niklas.bbtracker.mvvm.model.ExerciseModel;
import de.cramer.niklas.bbtracker.mvvm.model.SetModel;
import de.cramer.niklas.bbtracker.mvvm.model.WorkoutModel;
import de.niklascramer.mvvm.Property;
import de.niklascramer.mvvm.model.ListModel;
import de.niklascramer.mvvm.model.Model;
import de.niklascramer.util.collections.ObservableCollection;

public class SQLiteDataBaseController extends SQLiteOpenHelper implements IDataBase
{
	private static final int DATABASE_VERSION = 1;

	private static final String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS ";

	private static final String DATABASE_NAME = SQLiteDataBaseController.class.getName() + ".db";

	private static SQLiteDataBaseController instance;

	private static SQLiteDatabase database;

	private static Map<Class<?>, String> typeToKeyMap = new HashMap<>();

	private static String[] ignoreKeys = new String[]{"TIMESTAMP"};

	private static Object syncRoot = new Object();

	public SQLiteDataBaseController(Context context)
	{
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		database = getWritableDatabase();
		initMap();
		createTables();
		instance = this;
	}

	private void createTables()
	{
		createTable(new ExerciseModel());
		createTable(new SetModel());
		createTable(new WorkoutModel());
	}

	private void initMap()
	{
		typeToKeyMap.put(Enum.class, "TEXT");
		typeToKeyMap.put(String.class, "TEXT");

		typeToKeyMap.put(Integer.class, "INTEGER");
		typeToKeyMap.put(Long.class, "REAL");
		typeToKeyMap.put(Float.class, "REAL");
		typeToKeyMap.put(Double.class, "REAL");
	}

	private void createTable(Model model)
	{
		model.setup();

		Class type = model.getClass();

		StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append(CREATE_TABLE).append(type.getSimpleName()).append(" (");

		for (Property<?> property : model.getProperties()) {
			String name = property.getPropertyName().toUpperCase();

			boolean skip = false;

			for (String ignoreKey : ignoreKeys) {
				if (name.equalsIgnoreCase(ignoreKey)) {
					skip = true;
				}
			}

			if (skip) {
				continue;
			}

			final Class<?> valueType = property.get().getClass();
			String dbType = typeToKeyMap.get(valueType);

			if (dbType == null) {
				System.out.println("Assigning default key to: " + name);
				dbType = "TEXT";
			}
			queryBuilder.append(name).append(" ").append(dbType);
			if (isPrimaryKey(name)) {
				queryBuilder.append(" PRIMARY KEY AUTOINCREMENT");
			}
			queryBuilder.append(", ");
		}

		queryBuilder.replace(queryBuilder.lastIndexOf(","), queryBuilder.lastIndexOf(",") + 1, "");

		queryBuilder.append(");");

		String query = queryBuilder.toString();

		database.execSQL(query);
	}

	private boolean isPrimaryKey(final String name)
	{
		return name.equalsIgnoreCase("id");
	}

	public static SQLiteDataBaseController getInstance()
	{
		return instance;
	}

	public static SQLiteDataBaseController getInstance(Context context)
	{
		return instance != null ? instance : (instance = new SQLiteDataBaseController(context));
	}

	public List<ExerciseModel> getExercisesForDay(final Long date, IProgressListener progressListener)
	{
		List<ExerciseModel> result = new ArrayList<>();

		progressListener.onProgressChanged(this, 0);

		DateTime dateTime = new DateTime(date);

		long startOfDay = dateTime.withMillisOfDay(1).getMillis();

		long endOfDay = dateTime.withHourOfDay(23).withMinuteOfHour(59).withSecondOfMinute(59).withMillisOfSecond(999).getMillis();

		String projection = getProjection(new ExerciseModel().setup());

		try {
			final Cursor cursor = database.rawQuery("select " + projection + " from " + getTableName(ExerciseModel.class) + " where " + "DATE" + " BETWEEN " + startOfDay + " AND " + endOfDay + " ORDER BY " + "LISTINDEX" + " ASC ", null);

			final float columnCountExercises = cursor.getCount();

			float doneWork = 0;

			while (cursor.moveToNext()) {

				final ExerciseModel model = new ExerciseModel("", new DateTime(), null);

				fillProperties(model, cursor);

				// Get all sets for the Exercise
				final List<SetModel> sets = getSetsForExercise(model.getId());
				for (SetModel set : sets) {
					if (set.getSetIndex() != -1) {
						model.add(set);
					}
					else {
						model.setModelSetSum(set);
					}
				}
				result.add(model);
				doneWork++;

				progressListener.onProgressChanged(this, (int) (doneWork / columnCountExercises * 100f));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		progressListener.onProgressChanged(this, 100);

		return result;
	}

	private List<SetModel> getSetsForExercise(long exerciseId)
	{
		List<SetModel> result = new ArrayList<>();

		String projection = getProjection(new SetModel());

		Cursor cursor = database.rawQuery("SELECT " + projection + " FROM " + getTableName(SetModel.class) + " WHERE " + "EXERCISEINDEX" + " = " + exerciseId + " ORDER BY " + "SETINDEX" + " ASC ", null);

		while (cursor.moveToNext()) {
			final SetModel model = new SetModel();
			fillProperties(model, cursor);
			result.add(model);
		}

		return result;
	}

	private void fillValuesFromProperties(final Model model, final ContentValues values)
	{
		final List<Property<?>> properties = model.getProperties();
		for (Property<?> property : properties) {

			final String propertyName = property.getPropertyName().toUpperCase();

			boolean skip = false;

			for (String ignoreKey : ignoreKeys) {
				if (ignoreKey.equalsIgnoreCase(propertyName)) {
					skip = true;
				}
			}

			if (propertyName.equalsIgnoreCase("iD")) {
				skip = property.<Integer>getT() == -1;
			}

			if (skip) {
				continue;
			}

			final String typeName = property.getValueType().getSimpleName();
			switch (typeName) {
				case "Integer":
					values.put(propertyName, property.<Integer>getT());
					break;
				case "Long":
					values.put(propertyName, property.<Long>getT());
					break;
				case "Float":
					values.put(propertyName, property.<Float>getT());
					break;
				case "Double":
					values.put(propertyName, property.<Double>getT());
					break;
				case "String":
					values.put(propertyName, property.<String>getT());
					break;
				case "Enum":
					values.put(propertyName, property.get().toString());
				default:
					Log.e(getClass().getSimpleName(), "Cannot retrieve type: " + typeName);
					break;
			}
		}
	}

	private void fillProperties(final Model model, final Cursor cursor)
	{
		final List<Property<?>> properties = model.getProperties();

		for (Property<?> property : properties) {

			boolean skip = false;

			for (String ignoreKey : ignoreKeys) {
				if (ignoreKey.equalsIgnoreCase(property.getPropertyName())) {
					skip = true;
				}
			}

			if (skip) {
				continue;
			}

			final int columnIndex = cursor.getColumnIndex(property.getPropertyName().toUpperCase());

			final String typeName = property.getValueType().getSimpleName();
			switch (typeName) {
				case "Integer":
					property.set(cursor.getInt(columnIndex));
					break;
				case "Long":
					property.set(cursor.getLong(columnIndex));
					break;
				case "Float":
					property.set(cursor.getFloat(columnIndex));
					break;
				case "Double":
					property.set(cursor.getDouble(columnIndex));
					break;
				case "String":
					property.set(cursor.getString(columnIndex));
					break;
				case "Enum":
					//					Property.<Enum>castproperty.set(cursor.getString(columnIndex));
					break;
				default:
					Log.e(getClass().getSimpleName(), "Cannot retrieve type: " + typeName);
					break;
			}
		}
	}

	private String getProjection(Model model)
	{
		StringBuilder builder = new StringBuilder();

		for (Property<?> property : model.getProperties()) {

			boolean skip = false;

			for (String ignoreKey : ignoreKeys) {
				if (ignoreKey.equalsIgnoreCase(property.getPropertyName())) {
					skip = true;
				}
			}
			if (skip) {
				continue;
			}
			builder.append(" ");
			builder.append(property.getPropertyName().toUpperCase());
			if (model.getProperties().indexOf(property) < model.getProperties().size() - 2) {
				builder.append(",");
			}
		}

		return builder.toString();
	}

	private String getTableName(Class<?> type)
	{
		return type.getSimpleName();
	}

	@Override
	public void saveToDatabase(final Model model)
	{
		synchronized(syncRoot) {
			final String tableName = getTableName(model.getClass());

			ContentValues values = new ContentValues();

			fillValuesFromProperties(model, values);

			int result = (int) database.insertWithOnConflict(tableName, null, values, SQLiteDatabase.CONFLICT_IGNORE);

			if (result == -1) {
				values.remove("ID");
				database.update(tableName, values, "ID" + "=" + model.getId(), null);
			}
			else {
				model.setId(result);
			}
		}
	}

	@Override
	public void onCreate(SQLiteDatabase db)
	{
		database = db;
	}

	@Override
	public void deleteFromDataBase(final Model model)
	{

		if (model instanceof ListModel) {
			final ObservableCollection<Model> listChildModels = ((ListModel) model).getItems();
			for (Model child : listChildModels) {
				deleteFromDataBase(child);
			}
		}

		final String tableName = getTableName(model.getClass());
		database.execSQL("DELETE FROM " + tableName + " WHERE " + "iD" + " = " + model.getId() + ";");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
	{
		db.execSQL("DROP TABLE IF EXISTS " + getTableName(ExerciseModel.class));
		db.execSQL("DROP TABLE IF EXISTS " + getTableName(SetModel.class));
	}
}